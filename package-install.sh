#!/bin/bash

# packages required for work installations



USE_SUDO="sudo "
CUR_USER=$(whoami)
if [ "$CUR_USER" = "root" ]
then
	USE_SUDO=""
fi


# Update packages
$USE_SUDO$PACKAGE_MAN_UPDATE

# Try to install git and vim
$USE_SUDO$INSTALL git vim python-pip $SHELL

# extra install commands
$USE_SUDO$EXTRA_INSTALL

$USE_SUDO$FONT_INSTALL
