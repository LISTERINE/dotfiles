alias damnit='sudo "$BASH" -c "$(history -p !!)"'

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

# Recursively delete `.DS_Store` files
alias cleanup="find . -type f -name '*.DS_Store' -ls -delete"

# ROT13-encode text. Works for decoding, too! ;)
alias rot13='tr a-zA-Z n-za-mN-ZA-M'

# cookiecut a quickstack container project
alias quickstack="cookiecutter https://gitlab.com/LISTERINE/quickstack.git"

alias or="vagrant ssh -c 'sudo service uwsgi restart'"
alias ol="vagrant ssh -c 'sudo tail -F /var/log/uwsgi/app/overwatch.log'"
alias olf="vagrant ssh -c 'sudo tail -F -n0 /var/log/uwsgi/app/overwatch.log' |grep -A35 "
alias olt="vagrant ssh -c 'sudo tail -F -n0 /var/log/uwsgi/app/overwatch.log' |grep -A35 Traceback"
alias pdb="vagrant ssh -c 'netcat localhost 4444'"
alias op="vagrant ssh -c '/opt/overwatch/env_py3/bin/python'"

alias avault='ansible-vault --vault-password-file ~/.vault_pass.txt'

alias ow_update='for d in ./*/ ; do (cd "$d" && pwd && git checkout master && git pull); done'
alias ow_update_py3='for d in ./*/ ; do (cd "$d" && git pull && git co master-python3 && git pull origin master-python3); done'
alias ow_branches='for d in ./*/ ; do (if [[ $(git -C "$d" rev-parse --abbrev-ref HEAD) != "master" ]] then echo "$d"); done'
# alias ow_branches='for d in ./*/ ; do (cd "$d" && printf "%s " "${PWD##*/}" && git rev-parse --abbrev-ref HEAD); done'

alias vssh='vagrant ssh'

alias regen="gsrt_utilities_client project -rfi"

gitroot() {
	cd $(git rev-parse --show-toplevel)
}

#send command to everyone
hive() {
	python ~/.dotfiles/send_notify.py $@
}

# notify done
nd() {
	# Trap command line
	command="$@";
	# exectute command
	$@
	# record exit status
	cmdstatus=failed
    if [ $? -eq 0 ]; then
        cmdstatus=completed
    fi

	( (
      result=$(alerter -message "$cmdstatus: $command" -json -actions done -timeout 8 | python -c "import sys, json; print(json.load(sys.stdin)['activationType'])")
	  if [ "$result" = "timeout" ]; then
		  safeCommand=${command//\"/}
		  safeCommand=${safeCommand//\'/}
		  echo "$cmdstatus - $command" | mail 12078389355@tmomail.net
		  # hive --message "$status - $safeCommand"
		  say "Task $cmdstatus"
	  fi
    ) &)
}

wake() {
    make -C $(git rev-parse --show-toplevel) $@
}
