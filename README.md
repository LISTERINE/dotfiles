dotfiles
========
####My dotfiles

To install them using installer scripts:   

```bash
	$ git clone whereeveryougotthis
	$ mv ~/dotfiles ~/.dotfiles
	$ source ~/.dotfiles/install.sh
```

required font:
Shure Tech Mono Nerd Font Complete Mono.ttf (https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/ShareTechMono/complete/Shure%20Tech%20Mono%20Nerd%20Font%20Complete%20Mono.ttf)