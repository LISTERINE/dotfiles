#!/bin/sh
############################
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
# It will also determine current platform and allow package isntallation with pre and post install actions
############################

SHELL=$1
rc="rc"
if [ -z $SHELL ]
	then
	SHELL="zsh"
fi
rc="~/.$SHELL$rc"
export SHELL

. ./.dotfiles/platformer.sh

# Pre-package-install actions
if [ -e ./.dotfiles/pre-install.sh ]
    then
	echo "Running pre-install..."
    . ./.dotfiles/pre-install.sh
	echo "Pre-install complete"
fi
# Install any required packages for the environment
if [ -e ./.dotfiles/package-install.sh ]
    then
	echo "Running package install..."
	. ./.dotfiles/package-install.sh
	echo "Package install complete"
fi


########## Variables

dir=~/.dotfiles        # dotfiles directory
olddir=~/.dotfiles_old # old dotfiles backup directory
files="$extra_files zshrc bashrc vim vimrc bash_aliases bash_prompt tmux.conf venv-tools" # list of files/folders to symlink in homedir

##########

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"
echo

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"
echo

echo Symlinking vimrc
ln -s $dir/.vim/vimrc $dir/.vimrc
echo "...done"
echo

# move relevant existing dotfiles in homedir to dotfiles_old directory, then create symlinks
echo "Moving any existing dotfiles from ~ to $olddir"
for file in $files; do
    mv ~/.$file $olddir 2>/dev/null
    echo "Creating symlink to $file in home directory."
    ln -s $dir/.$file ~/.$file
done
echo "...done"
echo

# Once packages installed,
if [ -e ./.post-install.sh ]
    then
	echo "Running post-install..."
    . ./.dotfiles/post-install.sh
	echo "Post-install complete"
fi

echo "Running rc file to take care of any initialization. If this fails, install the font in the font direcotry"
$SHELL $rc

cd ~