pip install -r requirements.txt

# install neobundle once the propper dotfiles are in place
echo Getting neobundle
git clone https://github.com/Shougo/neobundle.vim.git
echo "...done"
echo

echo Installing neobundle
sh neobundle.vim/bin/install.sh
echo "...done"
echo

echo Running vim plugin setup
sh neobundle.vim/bin/neoinstall
echo "...done"
echo

# cleanup
echo Housekeeping
rm -rf neobundle.vim
echo "...done"
echo
