#!/bin/sh

# Determine platform
SYSTEM=`cat /etc/os-release | grep ^NAME`
UNAME=`uname -a`
# POSIX compliant substring search hack
if case "${SYSTEM}" in *"Arch Linux"*) true;; *) false;; esac then
	export platform=arch
elif case "${SYSTEM}" in *"Ubuntu"*) true;; *) false;; esac then
	export platform=ubuntu
elif case "${UNAME}" in *"Darwin"*) true;; *) false;; esac then
	export platform=darwin
fi

echo $platform


DEFAULT_PACKAGES="curl wget git vim"



case $platform in
debian)
    export SSL_CERT_DIR="/etc/ssl/certs/"
    export CA_CERT_DIR="/usr/local/share/ca-certificates/"
    export PACKAGE_MAN="apt-get"
    export INSTALL="$PACKAGE_MAN install -y"
    export PACKAGE_MAN_UPDATE="$PACKAGE_MAN update -y"
    export PACKAGE_CONF_DIR="/etc/apt/"
    export PACKAGE_CONF_FILE="apt.conf"
    export CA_UPDATER="update-ca-certificates"
	export PACKAGES="$DEFAULT_PACKAGES python-pip fontconfig"
    export extra_files="";
    ;;
arch)
    export SSL_CERT_DIR="/etc/ssl/certs/"
    export CA_CERT_DIR="/usr/local/share/ca-certificates/"
    export PACKAGE_MAN="pacman"
    export INSTALL="$PACKAGE_MAN -S"
    export PACKAGE_MAN_UPDATE="$PACKAGE_MAN -Syu"
    export PACKAGE_CONF_DIR=""
    export PACKAGE_CONF_FILE=""
    export CA_UPDATER="update-ca-certificates"
	export PACKAGES="$DEFAULT_PACKAGES python python-pip"
	export FONT_INSTALL="$PACKAGE_MAN -U ./dotfiles/font/shure-tech-mono-nerd-font-complete-mono-1.0.0-1-any.pkg.tar.xz"
	export FONT_CACHE=""
    export extra_files="";
    ;;
ubuntu)
    export SSL_CERT_DIR="/etc/ssl/certs/"
    export CA_CERT_DIR="/usr/local/share/ca-certificates/"
    export PACKAGE_MAN="apt-get"
    export INSTALL="$PACKAGE_MAN install -y"
    export PACKAGE_MAN_UPDATE="$PACKAGE_MAN update -y"
    export PACKAGE_CONF_DIR="/etc/apt/"
    export PACKAGE_CONF_FILE="apt.conf"
    export CA_UPDATER="update-ca-certificates"
	export PACKAGES="$DEFAULT_PACKAGES python-pip fontconfig"
    export extra_files="";
    ;;
centos)
    export SSL_CERT_DIR="/etc/ssl/certs/"
    export CA_CERT_DIR="/etc/pki/ca-trust/source/anchors/"
    export PACKAGE_MAN="yum"
    export INSTALL="$PACKAGE_MAN install -y"
    export PACKAGE_MAN_UPDATE="$PACKAGE_MAN update -y"
    export PACKAGE_CONF_DIR="/etc/"
    export PACKAGE_CONF_FILE="yum.conf"
    export CA_UPDATER="update-ca-trust"
	export PACKAGES="$DEFAULT_PACKAGES python-pip"
    export extra_files="";
    ;;
darwin)
    export SSL_CERT_DIR="/etc/ssl/certs/"
    export CA_CERT_DIR="/usr/local/share/ca-certificates/"
    export PACKAGE_MAN="brew"
    export INSTALL="$PACKAGE_MAN install"
    export PACKAGE_MAN_UPDATE="$PACKAGE_MAN update"
    export PACKAGE_CONF_DIR=""
    export PACKAGE_CONF_FILE=""
    export CA_UPDATER=""
	export PACKAGES="$DEFAULT_PACKAGES"
	export EXTRA_INSTALL="easy_install pip"
    export extra_files="";
    ;;
esac

echo $SSL_CERT_DIR
echo $CA_CERT_DIR
echo $PACKAGE_MAN
echo $INSTALL
echo $PACKAGE_CONF_DIR
echo $PACKAGE_CONF_FIL
echo $CA_UPDATER
echo $PACKAGES
echo $extra_files